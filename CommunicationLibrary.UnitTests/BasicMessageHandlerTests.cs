using System;
using Xunit;
using IoCommunication;
using NSubstitute;
using System.IO;
using System.Text;

namespace CommunicationLibrary.UnitTests
{
    public class BasicMessageHandlerTests
    {
        public class testType
        {
            public int messageID { get; set; }
            public int agentID { get; set; }
        }

        public class testParam
        {
            public int Data { get; set; }
            public string MoreData { get; set; }
        }
        public class TestControler : IController
        {
            [IoConnMethod(4)]
            public dynamic A(testType t, testParam p) { return new { param1 = "_" + p.MoreData, param2 = 10 + p.Data }; }
            [IoConnMethod(3)]
            public dynamic B(testType t, testParam p) { return new { param1 = "_" + p.MoreData, param2 = 10 + p.Data }; }
            [IoConnMethod(2)]
            public dynamic C(testType t, testParam p) { return new { param1 = "_" + p.MoreData, param2 = 10 + p.Data }; }
            [IoConnMethod(1)]
            public dynamic D(testType t, testParam p) { return new { param1 = "_" + p.MoreData, param2 = 10 + p.Data }; }

            [IoConnMethod(3)]
            public void vB(testType t, testParam p) {}
            [IoConnMethod(2)]                       
            public void vC(testType t, testParam p) {}
            [IoConnMethod(1)]                       
            public void vD(testType t, testParam p) {}

        }

        public class BasicMessageHandlerTestingHelper : BasicMessageHandler<testType>
        {
            public BasicMessageHandlerTestingHelper(params IController[] c) : base(c)
            {

            }

            public int RpcCount => _funcs.Count;
            public int PsCount => _actions.Count;
        }

        [Fact]
        public void ctor_AddControlerWithRpcAndPS_NoDelegatesIsCorrect()
        {
            IController c = new TestControler();
            BasicMessageHandlerTestingHelper bmh = new BasicMessageHandlerTestingHelper(c);
            Assert.Equal(4, bmh.RpcCount);
            Assert.Equal(3, bmh.PsCount);
        }


        [Fact]
        public void HandleMessage_RpcMessage_WritesToStream()
        {
            IController c = new TestControler();
            BasicMessageHandler<testType> bmh = new BasicMessageHandler<testType>(c);

            var result = bmh.HandleMessage("{\"messageID\": 4,\"agentID\": 635,\"payload\": { \"data\": 123,\"moreData\": \"asdf\" }}");

            Assert.Equal("{\"param1\":\"_asdf\",\"param2\":133}", result);
        }
    }
}
