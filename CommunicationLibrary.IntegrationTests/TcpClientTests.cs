using Microsoft.Extensions.Hosting;
using IoCommunication;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Text;
using Castle.Core.Logging;
using NSubstitute;
using System.Linq;
using System.Threading;

namespace CommunicationLibrary.IntegrationTests
{
    public class TcpClientTests
    {
        public class MessageWrapper
        {
            public int messageID { get; set; }
            public int agentID { get; set; }
        }

        private const string MESSAGE_SEND = "{\"messageID\": 4,\"agentID\": 635,\"payload\": { \"data\": 123,\"moreData\": \"asdf\" }}";
        private const string RETURN_MES = "TEST :)";
        private const string SEND_MES = "SENDMES";

        public class SocketListener
        {
            private static bool Started = false;
            private static StreamWriter _streamWriter;
            public static StreamWriter StreamWriter => Started ? _streamWriter : throw new Exception();
            public static void Start(int listenPort, Action<string> handle)
            {
                var server = new TcpListener(IPAddress.Parse("127.0.0.1"), listenPort);
                server.Start();
                Started = true;

                using var client = server.AcceptTcpClient();
                using var stream = client.GetStream();

                var reader = new StreamReader(stream, Encoding.UTF8);
                var writer = new StreamWriter(stream, new UTF8Encoding(false));
                _streamWriter = writer;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    Assert.Equal("\"" + RETURN_MES + "\"", line);
                    var line2 = reader.ReadLine();
                    Assert.Equal(SEND_MES, line2);
                }

            }
        }
        public static IHostBuilder CreateHostBuilder(BasicMessageHandler<MessageWrapper> bmh) =>
            Host.CreateDefaultBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<IMessageHandler>(bmh);
                    services.AddHostedService<IoCommunication.TcpClient>();
                });

        private readonly Task Server;
        IHost host;
        BasicMessageHandler<MessageWrapper> bmh;

        public class TestsControler : IController
        {
            [IoConnMethod(4)]
            public object Mehod4(MessageWrapper mw, object o) => RETURN_MES;
        }

        public TcpClientTests()
        {
            Server = Task.Run(() => SocketListener.Start(32323, (s) => { Assert.Equal("fff", s); }));
            bmh = new BasicMessageHandler<MessageWrapper>(new TestsControler());
            host = CreateHostBuilder(bmh).Build();
            host.Start();
        }

        [Fact]
        public async void MessageSent()
        {
            SocketListener.StreamWriter.WriteLine(MESSAGE_SEND);
            SocketListener.StreamWriter.Flush();
            Thread.Sleep(30);
            bmh.SendMessage(SEND_MES);
            Thread.Sleep(5);
            await host.StopAsync();
            await Server;
        }

    }
}
