﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;


namespace IoCommunication
{
    public class SendMessageEventArgs
    {
        public string Message { get; set; }
    }

    public delegate void SendMessageEventHandler(object sender, SendMessageEventArgs e);
    public interface IMessageHandler
    {
        public string HandleMessage(string message);
        public event SendMessageEventHandler SendMessageHandler;
    }

    public class TcpClient : BackgroundService
    {
        private readonly IMessageHandler _messageHandler;
        private readonly ILogger<TcpClient> _logger;
        private readonly string CsIP;
        private readonly int CsPort;
        private System.Net.Sockets.TcpClient _client;

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            return base.StartAsync(cancellationToken);
        }
        public override void Dispose()
        {
            _client?.Dispose();
            base.Dispose();
        }
        public TcpClient(
            IMessageHandler messageHandler,
            ILogger<TcpClient> logger,
            IConfiguration configuration)
        {
            this._messageHandler = messageHandler;
            this._logger = logger;
            this.CsIP = configuration.GetValue<string>("CsIP", "127.0.0.1");
            this.CsPort = configuration.GetValue<int>("CsPort", 32323);
        }

        private StreamWriter StreamWriter;

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        { 

            try
            {
                _client = new System.Net.Sockets.TcpClient(CsIP, CsPort);
                using var stream = _client.GetStream();

                _messageHandler.SendMessageHandler += SendMessage;
                cancellationToken.Register(() => _client.Close());


                _logger.LogInformation($"{nameof(TcpClient)} connected {CsIP}:{CsPort}");

                var reader = new StreamReader(stream, Encoding.UTF8);
                StreamWriter = new StreamWriter(stream, new UTF8Encoding(false));

                await Task.Run(() => MainLoop(reader, StreamWriter), cancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "");
                throw;
            }
            finally
            {
                _messageHandler.SendMessageHandler -= SendMessage;
            }
        }

        private void SendMessage(object sender, SendMessageEventArgs e)
        {
            lock (StreamWriter)
            {
                StreamWriter.WriteLine(e.Message);
                StreamWriter.Flush();
            }
        }

        private void MainLoop(
            StreamReader reader,
            StreamWriter writer)
        {
            try
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    _logger.LogDebug($"Received {line}");

                    var tData = new ThreadData(writer, line);
                    ThreadPool.QueueUserWorkItem(HandleMessage, (object)tData);
                }
            }
            catch (Exception) { }
        }

        private class ThreadData
        {
            public StreamWriter Writer { get; }
            public string Message { get; }
            public ThreadData(StreamWriter stream, string message)
            {
                Writer = stream;
                Message = message;
            }
        }

        public void HandleMessage(object data)
        {
            try
            {
                var tData = data as ThreadData;
                var response = _messageHandler.HandleMessage(tData.Message);
                if(!(response is null))
                {
                    lock(tData.Writer)
                    {
                        tData.Writer.WriteLine(response);
                        tData.Writer.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Request handling error");
            }
        }
    }
}
