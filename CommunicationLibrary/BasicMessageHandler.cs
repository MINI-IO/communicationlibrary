﻿using System;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using Newtonsoft.Json.Serialization;

namespace IoCommunication
{
    public interface IController { }

    public interface IMessageSender
    {
        public void SendMessage(string mess);
        public void SendMessage(object mess);
    }

    public class BasicMessageHandler<TMessageWrapper> : IMessageHandler, IMessageSender
    {
        protected Dictionary<int, (Type type, Action<TMessageWrapper, object> action)> _actions
            = new Dictionary<int, (Type, Action<TMessageWrapper, object>)>();

        protected Dictionary<int, (Type type, Func<TMessageWrapper, object, object> func)> _funcs
            = new Dictionary<int, (Type, Func<TMessageWrapper, object, object>)>();

        private readonly JsonSerializer _serializer;

        public event SendMessageEventHandler SendMessageHandler;

        public BasicMessageHandler(params IController[] ioControllers)
        {
            _serializer = new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Include,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            };

            foreach (var ioController in ioControllers)
            {
                var type = ioController.GetType();
                if(type.GetInterfaces().Contains(typeof(IDisposable)))
                {
                    throw new ArgumentException(
                        $"Types implementing {nameof(IDisposable)} " +
                        $"are not allowed: {nameof(type)} !");
                }

                foreach(var method in type.GetMethods())
                {
                    foreach(
                        var ca in
                        method.GetCustomAttributes<IoConnMethod>()
                            .Where(ca => !(ca is null)))
                    {
                        if (method.GetParameters().Count() != 2)
                        {
                            throw new ArgumentException(
                                $"{nameof(IoConnMethod)} Methods should take 2 arguments");
                        }
                        if (method.GetParameters()[0].ParameterType != typeof(TMessageWrapper))
                        {
                            throw new ArgumentException(
                                $"{nameof(IoConnMethod)} Methods first argument " +
                                $"should be ${nameof(TMessageWrapper)}");
                        }

                        if (method.ReturnType == typeof(void))
                        {
                            _actions.Add(
                                ca.Routing,
                                (method.GetParameters()[1].ParameterType,
                                (r, l) => method.Invoke(ioController, new object[] { r, l })));
                        }
                        else
                        {
                            _funcs.Add(
                                ca.Routing,
                                (method.GetParameters()[1].ParameterType,
                                (r, l) => method.Invoke(ioController, new object[] { r, l })));
                        }
                    }
                }
            }
        }

        public void SendMessage(string mess)
        {
            this.SendMessageHandler.Invoke(this, new SendMessageEventArgs() { Message = mess });
        }

        public void SendMessage(object mess)
        {
            StringBuilder stringBuilder = new StringBuilder();
            StringWriter stringWriter = new StringWriter(stringBuilder);
            _serializer.Serialize(stringWriter, mess);
            this.SendMessageHandler.Invoke(
                this,
                new SendMessageEventArgs() { Message = stringBuilder.ToString() });
        }


        public string HandleMessage(string message)
        {
            var obj = JObject.Parse(message);
            var messageContext = obj.ToObject<TMessageWrapper>();
            if (_actions.TryGetValue(obj.GetValue("messageId", StringComparison.OrdinalIgnoreCase).Value<int>(), out var action))
            {
                var payload = obj["payload"]?.ToObject(action.type);
                action.action.DynamicInvoke(messageContext, payload);
            }
            if(_funcs.TryGetValue(obj.GetValue("messageId", StringComparison.OrdinalIgnoreCase).Value<int>(), out var func))
            {
                var payload = obj["payload"]?.ToObject(func.type);
                var returnValue = func.func.DynamicInvoke(messageContext, payload);
                StringBuilder stringBuilder = new StringBuilder();
                StringWriter stringWriter = new StringWriter(stringBuilder);
                _serializer.Serialize(stringWriter, returnValue);
                return stringBuilder.ToString();
            }
            return null;
        }
    }
}
