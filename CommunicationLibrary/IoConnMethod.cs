﻿using System;

namespace IoCommunication
{
    [AttributeUsage(AttributeTargets.Method) ]
    public class IoConnMethod : Attribute
    {
        
        public int Routing { get; }
        public IoConnMethod(int routing)
        {
            this.Routing = routing;
        }
    }
}
